# 5DATA Sample Dataset Generator

## QRD

This script's goal is to generate a (mildly nonsensical) dataset for use with our project.

Since the goal is to ingest it in a MongoDB, each students would get a JSON record that looks like this:
```json
{
    "supId": "625155",
    "name": "Lucas",
    "lastname": "Fabre",
    "email": "lucas.fabre@supinfo.com",
    "birthdate": "28/06/1999",
    "address": {
        "street": "16, chemin de Carre",
        "city": "Paris",
        "country": "France"
    },
    "education": [
        {
            "schoolname": "Lyc\u00e9e Emile Zola",
            "degrees": "Master"
        },
        {
            "schoolname": "ENI",
            "degrees": "Master"
        }
    ],
    "internships": [],
    "sup_cursus": [
        {
            "class_name": "BoE3",
            "campus": "Paris",
            "modules": [
                {
                    "name": "RGPD",
                    "grades": 4
                },
                {
                    "name": "RBIG",
                    "grades": 13
                },
                {
                    "name": "CHGM",
                    "grades": 18
                }
            ],
            "attendance": 12,
            "contract": {},
            "attended_open_day": false
        }
    ],
    "post_cursus": {
        "future_job": "",
        "quit_reason": "D\u00e9m\u00e9nagement",
        "new_school": "Epitech"
    }
}
```

## Quick-start

* Install the required libraries (preferably in a virtual env):
```
pip install -r requirements.txt
```

* Run the script:
```
python student.py
```

By default, the script generates a single student and outputs to stdout.

Arguments can be listed with `--help`:
```
usage: main.py [-h] [-c COUNT] [-o OUTFILE]

optional arguments:
  -h, --help            show this help message and exit
  -c COUNT, --count COUNT
                        Amount of students to generate
  -o OUTFILE, --outfile OUTFILE
                        Output file
```

The `main.py` script is a simple web server that can generate students on-demand once started.

Simply `GET /generate_student`.

## Docker

A convenient Docker image is provided through Gitlab's registry.

### Quick usage

Run the following:
```
docker login registry.gitlab.com
docker pull registry.gitlab.com/5data-paris-2122/dataset-generator:latest
docker run -p 80:8080 -d registry.gitlab.com/5data-paris-2122/dataset-generator:latest
curl http://127.0.0.1:8080/generate_student
```