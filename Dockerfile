FROM python:3.9-alpine3.15

WORKDIR /srv/

# Copy requirements.txt and install first for better cache on later pushes
COPY requirements.txt requirements.txt
RUN pip install -r requirements.txt

COPY . ./

# TODO? Change run as user

CMD ["python","-u","main.py"]