import sys
from http.server import HTTPServer, SimpleHTTPRequestHandler

from student import Student

PORT = 8080


class PythonServer(SimpleHTTPRequestHandler):
    """Python HTTP Server that handles GET and POST requests"""

    def end_headers(self):
        self.send_header('Access-Control-Allow-Origin', '*')
        self.send_header('Access-Control-Allow-Methods', 'GET')
        self.send_header('Content-Type', 'application/json')
        SimpleHTTPRequestHandler.end_headers(self)

    def do_GET(self):
        if self.path == '/generate_student':
            student = Student()
            self.send_response(200, "OK")
            self.end_headers()
            self.wfile.write(bytes(student.outputAsJson(), "utf-8"))
        elif self.path == '/':
            self.send_response(200, "OK")
            self.end_headers()
            self.wfile.write(bytes("Try /generate_student", "utf-8"))
        else:
            self.send_response(404, "Not found")
            self.end_headers()
            self.wfile.write(bytes("404 Not Found", "utf-8"))


if __name__ == "__main__":
    server = HTTPServer(("0.0.0.0", PORT), PythonServer)
    print("Server started")

    try:
        server.serve_forever()
    except KeyboardInterrupt:
        server.server_close()
        print("Server stopped successfully")
        sys.exit(0)
