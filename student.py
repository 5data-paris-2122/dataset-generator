#!/usr/bin/env python3

import argparse
import sys
import json
import random
from faker import Faker
from unidecode import unidecode
from datetime import datetime

"""Dataset Generator
"""

fake = Faker('fr-FR')

# Pre-set dictionnaries
# Addresses
pre_defined_cities = ["Paris", "Rennes", "Caen", "Lyon", "Marseille"]
# Education
pre_defined_schools = ["Lycée Jean Jaurès", "Epitech", "ENI", "Université de Paris", "CNAM",
                       "Lycée Emile Zola", "Lycée Victor et Hélène Basch", "Université de Rennes"]
pre_defined_degrees = ["BAC S", "BAC Pro", "BAC L", "BTS", "Licence Pro", "Licence", "Master"]
# Internships & Contrats pro
pre_defined_companies = ["IONIS", "Thales", "Hermes", "Google", "Microsoft", "Bouygues Telecom", "Orange",
                         "SFR", "Apple", "General Electrics", "Ministère de l'Écologie", "Banque de France", "Engie",
                         "SNCF", "Safran", "Nexter", "RATP"]
# Cursus
pre_defined_cursus = ["BoE1", "BoE2", "BoE3", "MoE1", "MoE2"]
pre_defined_modules = ["DATA", "DOKR", "KUBE", "RGPD", "CHGM", "ITIL", "ARCH", "CCNA", "OENT", "MLSP", "PENE", "VIRT", "RBIG"]
pre_defined_jobs = ["Ingénieur Système", "Scrum Master", "Architecte", "Chef de Projet",
                    "Ingénieur DevOps", "Ingénieur Cybersécurité", "Analyste en Cybersécurité"]
# Post-cursus
pre_defined_quit_reasons = ["Maladie", "Réorientation", "Déménagement", "Mobilité Professionnelle"]


class Student():
    def __init__(self):
        self.id = self.__genID()
        self.name, self.lastname = self.__genName()
        self.email = self.__genEmail(self.name, self.lastname)
        self.birthdate = self.__genDOB()
        self.address = self.__genFullAddress()
        self.education = self.__genEducation()
        self.internships = self.__genInternships()
        self.sup_cursus = self.__genCursus(self.address['city'])
        self.post_cursus = self.__genPostCursus()

    def __genID(self):
        """Generates 6 digit random ID. No duplicate check, so technically there's a chance we get two different students with the same ID...
        But it IS a PoC after all."""
        return ''.join([str(random.randint(1, 6)) for i in range(0, 6)])

    def __genName(self):
        """Use Faker to generate a random full name"""
        name = fake.first_name()
        lastname = fake.last_name()
        return name, lastname

    def __genEmail(self, name, lastname):
        """Format email from full name"""
        email = "{0}.{1}@supinfo.com".format(name, lastname).lower()
        # Strip accents
        return unidecode(email)

    def __genDOB(self):
        """Generate semirandom date of birth"""
        potential_birthdate_start = datetime.strptime('01/01/1990', '%d/%m/%Y')  # Only 90s kids
        potential_birthdate_end = datetime.strptime('01/01/2000', '%d/%m/%Y')
        birthdate = potential_birthdate_start + (potential_birthdate_end - potential_birthdate_start) * random.random()
        return birthdate.strftime('%d/%m/%Y')

    def __genFullAddress(self):
        """Uses Faker and pre-declared dicts to generate mildly nonsensical full addresses"""
        address = dict()
        address['street'] = fake.street_address()
        address['city'] = pre_defined_cities[random.randint(0, len(pre_defined_cities) - 1)]
        address['country'] = "France"
        return address

    def __genEducation(self):
        """Generate a completely nonsensical education curriculum"""
        education = list()
        # Let's say each students can have between 0 and 2 degrees from past scholarships
        for amount_of_degrees in range(random.randint(0, 2)):
            schoolname = pre_defined_schools[random.randint(0, len(pre_defined_schools) - 1)]
            degrees = pre_defined_degrees[random.randint(0, len(pre_defined_degrees) - 1)]
            education.append({"schoolname": schoolname, "degrees": degrees})
        return education

    def __genInternships(self):
        """Generate wildly incoherent internships"""
        internships = list()
        # Let's pretend each students can have between 0 and 2 internships
        for amount_of_internships in range(random.randint(0, 2)):
            # There can be duplicates... Not sure whether or not it's a bad thing
            company_name = pre_defined_companies[random.randint(0, len(pre_defined_companies) - 1)]
            # Do note that years are fully random and do not take into account previous internships/current gigs
            potential_internship_start = datetime.strptime('2010', '%Y')
            potential_internship_end = datetime.strptime('2015', '%Y')
            year = potential_internship_start + (potential_internship_end - potential_internship_start) * random.random()
            internships.append({"company_name": company_name, "year": year.strftime('%Y')})
        return internships

    def __genCursus(self, city):
        """Generate a relatively sensible Supinfo student status"""
        class_name = pre_defined_cursus[random.randint(0, len(pre_defined_cursus) - 1)]
        # Students usually live near their campus. Usually
        campus = city

        modules = list()
        # Three modules per students
        for amount_of_modules in range(3):
            # Make sure module_name is unique
            unique = False
            while not unique:
                module_name = pre_defined_modules[random.randint(0, len(pre_defined_modules) - 1)]
                if module_name not in modules:
                    unique = True

            grades = random.randint(0, 20)
            modules.append({'name': module_name, 'grades': grades})

        # Students can miss up to 15 days and then they're FIRED
        attendance = random.randint(0, 15)

        # Coinflip whether or not the student has an ongoing contract
        if random.randint(0, 1) == 1:
            company_name = pre_defined_companies[random.randint(0, len(pre_defined_companies) - 1)]
            # Add "Apprenti" before the job title, since it's an apprenticeship
            job_title = "Apprenti {}".format(pre_defined_jobs[random.randint(0, len(pre_defined_jobs) - 1)])
            contract = {'job_title': job_title, 'company_name': company_name}
        else:
            contract = dict()

        # Coinflip whether or not the student visited the school before entering it
        if random.randint(0, 1) == 1:
            attended_open_day = True
        else:
            attended_open_day = False

        # Pack it up
        sup_cursus = {
            "class_name": class_name,
            "campus": campus,
            "modules": modules,
            "attendance": attendance,
            "contract": contract,
            "attended_open_day": attended_open_day
        }

        # Put it as a list. #4
        return [sup_cursus]

    def __genPostCursus(self):
        """Generate a few attributes that tell what the student did if they left the school during the year"""
        # If the student is undergoing their last year of school, add a future job property
        if self.sup_cursus[0]['class_name'] == "MoE2":
            company_name = pre_defined_companies[random.randint(0, len(pre_defined_companies) - 1)]
            job_title = pre_defined_jobs[random.randint(0, len(pre_defined_jobs) - 1)]
            future_job = {'job_title': job_title, 'company_name': company_name}
            quit_reason = "Finished studies"
            new_school = ""
        else:
            # Let's say about 1 student out of 10 drops out
            if random.randint(0, 9) == 1:
                future_job = dict()
                quit_reason = pre_defined_quit_reasons[random.randint(0, len(pre_defined_quit_reasons) - 1)]
                new_school = pre_defined_schools[random.randint(0, len(pre_defined_schools) - 1)]
            else:
                future_job = dict()
                quit_reason = ""
                new_school = ""

        # Pack it up
        post_cursus = {
            "future_job": future_job,
            "quit_reason": quit_reason,
            "new_school": new_school
        }

        return post_cursus

    def outputAsJson(self, file=None):
        """Create a dictionnary with every attributes and return it as JSON"""
        packed = {
            "supId": self.id,
            "name": self.name,
            "lastname": self.lastname,
            "email": self.email,
            "birthdate": self.birthdate,
            "address": self.address,
            "education": self.education,
            "internships": self.internships,
            "sup_cursus": self.sup_cursus,
            "post_cursus": self.post_cursus
        }
        return json.dumps(packed, indent=4)


def main(arguments):
    parser = argparse.ArgumentParser(
        description=__doc__,
        formatter_class=argparse.RawDescriptionHelpFormatter)
    parser.add_argument('-c', '--count', help="Amount of students to generate",
                        default=1)
    parser.add_argument('-o', '--outfile', help="Output file",
                        default=sys.stdout, type=argparse.FileType('w'))

    args = parser.parse_args(arguments)

    for amount in range(int(args.count)):
        student = Student()
        # If there's more than one we'll need a comma after each json objects, til the last
        if amount + 1 != int(args.count):
            sep = ',\n'
        else:
            sep = str()
        args.outfile.write(student.outputAsJson() + sep)


if __name__ == '__main__':
    sys.exit(main(sys.argv[1:]))
